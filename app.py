from app.event_parser import EventParser
from app.models import StreamEvents

if __name__ == '__main__':
    # create events data
    items = [
        'I just won a lottery #update @all',
        'I just won a lottery #update @all-friends',
        'I just won a lottery #warn @john',
        'I just won a lottery #update @all',
        'I just won a second lottery #update @john',
        'I just won a lottery #poll @all'
    ]

    stream = StreamEvents()
    for it in items:
        stream.add(EventParser.parse_to_event(it))

    # filter events by category='update'
    top_update = stream.filter(field_name='category', field_value='update')
    print("Top category='update':")
    print(top_update)
    print('')

    # get top any events
    top_any = stream.filter(n=2)
    print("Top any 'n' events:")
    print(top_any)
    print('')

    # filter events by person='john'
    top_john_person = stream.filter(field_name='person', field_value='john')
    print("Top 'john' events:")
    print(top_john_person)
    print('')
