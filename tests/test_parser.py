import unittest
from app.event_parser import EventParser


class EventParserTests(unittest.TestCase):

    def test_parse_title(self):
        event_string = 'I just won a lottery #update @all'
        title = EventParser.parse_title(event_string)
        self.assertEqual(title, 'I just won a lottery')

    def test_parse_category(self):
        event_string = 'I just won a lottery #update @all'
        category = EventParser.parse_category(event_string)
        self.assertEqual(category, 'update')

    def test_parse_person(self):
        event_string = 'I just won a lottery #update @all'
        person = EventParser.parse_person(event_string)
        self.assertEqual(person, 'all')


if __name__ == '__main__':
    unittest.main()
