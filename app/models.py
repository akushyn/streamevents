from datetime import datetime


class EventCategory:
    UPDATE = 'update'
    POLL = 'poll'
    WARN = 'warn'


class StreamEvent:
    def __init__(self, title, category=EventCategory.UPDATE, person='all'):
        self.title = title
        self.category = category
        self.person = person
        self.time = datetime.now()

    def __repr__(self):
        return "{}({}, {}, {}, {})".format(self.__class__.__name__, self.title, self.category, self.person, self.time)


class StreamEvents:
    def __init__(self):
        self._events = []

    def get_events(self):
        return self._events

    def add(self, event):
        """
        Add a new event
        :param event: The event object
        """
        self._events.append(event)

    def filter(self, field_name=None, field_value=None, n: int = 10):
        """
        Filter events by 'field_name' with 'field_value' value
        :param field_name: The possible field name attribute
        :param field_value: The value of the field_name
        :param n: The length of first top events
        """
        if field_name and field_value:
            assert field_name in ['category', 'person', 'time']

            filtered = [e for e in self._events if getattr(e, field_name) == field_value]
            return filtered[:n]

        return self._events[:n]

    def __repr__(self):
        s = ''
        for e in self._events:
            s = s + repr(e)
        return s
