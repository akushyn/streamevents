import re

from app.models import StreamEvent


class EventParser:
    """
    Class to parse input event string
    """

    @classmethod
    def parse_to_dict(cls, event_string):
        """
        Parse event string to dict
        """
        title = cls.parse_title(event_string)
        category = cls.parse_category(event_string)
        person = cls.parse_person(event_string)

        return {
            "title": title,
            "category": category,
            "person": person
        }

    @classmethod
    def parse_to_event(cls, event_string: str):
        """
        Parse event string to tokens
        """
        parsed_data = cls.parse_to_dict(event_string)
        return StreamEvent(
            title=parsed_data.get('title'),
            category=parsed_data.get('category'),
            person=parsed_data.get('person')
        )

    @classmethod
    def parse_category(cls, event_string: str):
        """
        Parse category
        """
        return cls._find_match(event_string, operator='#')

    @classmethod
    def parse_person(cls, event_string):
        """
        Parse person
        """
        return cls._find_match(event_string, operator='@')

    @classmethod
    def parse_title(cls, event_string):
        """
        Parse title
        """
        # TODO: possible smarter decision to extract title
        return event_string.split('#')[0].strip()

    @classmethod
    def _find_match(cls, value, operator):
        regex = re.compile(r"{}(\S+)".format(operator))
        search = regex.search(value)

        if not search:
            return ''

        return search.group(1)
